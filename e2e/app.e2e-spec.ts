import { AboutMePage } from './app.po';

describe('about-me App', () => {
  let page: AboutMePage;

  beforeEach(() => {
    page = new AboutMePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
