var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

/*app.use(express.static(__dirname + '/dist/inline.bundle.js'));
app.use(express.static(__dirname + '/dist/inline.bundle.js.map'));
app.use(express.static(__dirname + '/dist/main.bundle.js'));
app.use(express.static(__dirname + '/dist/main.bundle.js.map'));
app.use(express.static(__dirname + '/dist/polyfills.bundle.js'));
app.use(express.static(__dirname + '/dist/polyfills.bundle.js.map'));
app.use(express.static(__dirname + '/dist/styles.bundle.js'));
app.use(express.static(__dirname + '/dist/styles.bundle.js.map'));
app.use(express.static(__dirname + '/dist/vendor.bundle.js'));
app.use(express.static(__dirname + '/dist/vendor.bundle.js.map'));*/

app.use(express.static(__dirname + '/dist'));
//app.use(express.static(__dirname + '/dist/images'));

app.get('*', function(req, res) {
  res.sendFile(__dirname + '/dist/index.html');
});

io.on('connection', function(socket){
  socket.on('set question', function(question) {
    console.log('Set question', question);
    io.emit('set question', question);
  });

  socket.on('clear', function() {
    console.log('Clearing...');
    io.emit('clear');
  });

  socket.on('reveal', function() {
    console.log('Reveal...');
    io.emit('reveal');
  });

  socket.on('hide', function() {
    console.log('Hide...');
    io.emit('hide');
  });

  socket.on('project image', function(image) {
    console.log('Project image', image);
    io.emit('project image', image);
  });

  socket.on('remove projection', function(image) {
    console.log('Remove projection...');
    io.emit('remove projection');
  });

  socket.on('start', function(image) {
    console.log('Start...');
    io.emit('start');
  });

  socket.on('toggle user answers', function(status) {
    console.log('Toggle user answers...', status);
    io.emit('toggle user answers');
  });
});

http.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
