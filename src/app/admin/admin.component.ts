import * as io from 'socket.io-client';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

import { QUESTIONS } from '../question/questions';
import { IMAGES } from '../images';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  questions = QUESTIONS;
  images = IMAGES;

  activeQuestion = null;
  activeImage = null;

  socket = null;

  constructor(public afDb: AngularFireDatabase, public afAuth: AngularFireAuth, public router: Router) {
    this.afAuth.authState.subscribe(auth => {
      if (auth) {
        /*if (auth.uid !== 'CiVRf9NMkTeygj2zWapwQxdN6F43') {
          this.router.navigate(['/login']);
        }*/
      }
      else {
        this.router.navigate(['/login']);
      }
    });
  }

  ngOnInit() {
    this.socket = io(environment.socketServer);
  }

  reset() {
    this.activeImage = null;
    this.afDb.object('/answers').remove();
  }

  clear() {
    this.reset();
    this.socket.emit('clear');
  }

  next() {
    let question = null;
    let openQuestions = [];
    let i = 0;

    console.log('All questions', this.questions);

    // Collect all unanswered questions.
    for (i = 0; i < this.questions.length; i++) {
      // Also set the status of the current question.
      if (this.activeQuestion && this.activeQuestion.id == this.questions[i].id) {
         this.questions[i]['status'] = true;
      }

      if (!this.questions[i]['status'] || this.questions[i]['status'] === undefined) {
        openQuestions.push(this.questions[i]);
      }
    }

    console.log('Open questions', openQuestions);

    this.activeQuestion = null;

    if (openQuestions.length) {
      let questionIndex = Math.floor(Math.random() * openQuestions.length);
      question = openQuestions[questionIndex];
      this.activeQuestion = question;
    }

    console.log('Next question', question);

    this.reset();
    this.socket.emit('set question', question);
  }

  reveal() {
    this.socket.emit('reveal');
  }

  hide() {
    this.socket.emit('hide');
  }

  showImage(image) {
    this.activeImage = image;
  }

  closeImage() {
    this.activeImage = null;
  }

  projectImage(image) {
    this.socket.emit('project image', image);
  }

  removeProjection() {
    this.activeImage = null;
    this.socket.emit('remove projection');
  }

  start() {
    this.socket.emit('start');
  }

  showUserAnswers() {
    this.socket.emit('toggle user answers', true);
  }

  hideUserAnswers() {
    this.socket.emit('toggle user answers', false);
  }
}
