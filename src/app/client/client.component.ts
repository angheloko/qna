import * as io from 'socket.io-client';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import * as firebase from 'firebase/app';

import { QUESTIONS } from '../question/questions';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  questions = QUESTIONS;

  hasStarted = false;

  photo = null;
  uid = null;

  activeChoice = null;
  activeQuestion = null;
  activeImage = null;

  noQuestions = false;

  answer: FirebaseObjectObservable<any>;

  socket = null;

  constructor(public afAuth: AngularFireAuth, public afDb: AngularFireDatabase, public router: Router) {

  }

  ngOnInit() {
    this.photo = '';
    this.afAuth.authState.subscribe(auth => {
      if (auth) {
        this.uid = auth.uid;
        this.photo = auth.providerData[0].photoURL;
      }
      else {
        this.router.navigate(['/login']);
      }
    });

    this.socket = io(environment.socketServer);

    this.socket.on('set question', (question) => this.setQuestion(question));
    this.socket.on('clear', () => this.clear());
    this.socket.on('project image', (image) => this.projectImage(image));
    this.socket.on('remove projection', () => this.removeProjection());

    this.socket.on('start', () => {
      this.hasStarted = true;
    });
  }

  projectImage(image) {
    this.activeImage = image;
  }

  removeProjection() {
    this.activeImage = null;
  }

  clear() {
    this.activeQuestion = null;
    this.activeChoice = null;
    this.activeImage = null;
  }

  setQuestion(question) {
    this.clear();

    this.hasStarted = true;

    if (question) {
      this.activeQuestion = question;
      this.noQuestions = false;
    }
    else {
      this.activeQuestion = null;
      this.noQuestions = true;
    }
  }

  setChoice(choice) {
    this.activeChoice = choice;

    for (var i = 0; i < this.activeQuestion.choices.length; i++) {
      this.afDb.object('/answers/' + this.activeQuestion.choices[i].value + '/' + this.uid).remove();
    }

    if (this.activeQuestion) {
      this.afDb.object('/answers/' + choice.value + '/' + this.uid).set(true);
    }
  }

  onClickChoice(choice) {
    this.setChoice(choice);
  }

  logout() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(['/login']);
    });
  }
}
