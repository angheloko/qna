import * as io from 'socket.io-client';

import { Component, OnInit } from '@angular/core';

import { AngularFireDatabase, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2/database';

import { QUESTIONS } from './questions';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {
  questions = QUESTIONS;

  hasStarted = false;

  activeQuestion = null;
  activeImage = null;

  noQuestions = false;

  showCorrectAnswer = false;
  showAnswers = false;

  socket = null;

  answers = {};
  users = {};
  userAnswers: FirebaseListObservable<any>;

  constructor(public afDb: AngularFireDatabase) { }

  ngOnInit() {
    this.socket = io(environment.socketServer);

    this.socket.on('set question', (question) => this.setQuestion(question));
    this.socket.on('clear', () => this.clear());
    this.socket.on('reveal', () => this.revealAnswer());
    this.socket.on('hide', () => this.hideAnswer());
    this.socket.on('project image', (image) => this.projectImage(image));
    this.socket.on('remove projection', () => this.removeProjection());

    this.socket.on('start', () => {
      this.hasStarted = true;
    });

    this.socket.on('toggle user answers', (status) => {
      this.showAnswers = status;
    })
  }

  setQuestion(question) {
    this.clear();

    this.hasStarted = true;

    if (question) {
      this.activeQuestion = question;
      this.noQuestions = false;

      this.activeQuestion.choices.forEach((choice) => {
        this.afDb.list('/answers/' + choice.value).subscribe((snapshot) => {
          this.answers[choice.value] = snapshot.length;
        });
      });
    }
    else {
      this.activeQuestion = null;
      this.noQuestions = true;
    }
  }

  projectImage(image) {
    this.activeImage = image;
  }

  removeProjection() {
    this.activeImage = null;
  }

  clear() {
    this.activeQuestion = null;
    this.activeImage = null;
    this.noQuestions = false;
    this.showCorrectAnswer = false;
    this.showAnswers = false;
  }

  revealAnswer() {
    if (this.activeQuestion) {
      this.showCorrectAnswer = true;
    }
  }

  hideAnswer() {
    this.showCorrectAnswer = false;
  }

  getName(uid) {
    return this.afDb.object('/users/' + uid);
  }
}
